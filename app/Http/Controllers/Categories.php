<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use \App\Categoria;
use \App\CategoriaCentros;
use \App\Centro;

class Categories extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $idUsuario = auth()->user()->id;
        $centros = Centro::where('idUsuario', '=', $idUsuario)->get();
        $categories = Categoria::where('idUsuario', '=', $idUsuario)->get();
        return view('category')->with(['category' => true, 'categories' => $categories, 'centros' => $centros]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'descripcion' => 'required|max:150',
          // 'descripcion' => 'unique:categorias,descripcion',
          'descripcion' => Rule::unique('categorias')->where(function ($query) {
              $query->where('idUsuario', auth()->user()->id);
          }),
          'color' => 'required|max:7',
        ]);
        $category = new Categoria();
        $category->descripcion = $request->descripcion;
        $category->color = $request->color;
        $category->idUsuario = auth()->user()->id;
        if ($category->save()) {

        }
        else {

        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $this->validate($request, [
        'id' => 'required',
        'descripcion' => 'required|max:150',
        'color' => 'required|max:7',
      ]);
      $category = Categoria::find($request->id);
      $category->descripcion = $request->descripcion;
      $category->color = $request->color;
      if ($category->save()) {

      }
      else {

      }
      return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $this->validate($request, [
        'id' => 'required|numeric'
      ]);

      $categoriaCentros = CategoriaCentros::where('idCategoria', $request->id)->get();
      if(count($categoriaCentros) > 0) {
        $categories = Categoria::all();
        return view('category')->with([
          'category' => true,
          'categories' => $categories,
          'alert' => 'Esta categoía esta relacionada a centros, primero se debe eliminar el centro.',
        ]);
      } else {
        Categoria::destroy($request->id);
      }
      return back();
    }
}
