<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Centro;

use \App\Categoria;

use \DB;

use \App\CategoriaCentros;

class Centros extends Controller
{

    public function __construct()
    {
      $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $idUsuario = auth()->user()->id;
      $centros = Centro::where('idUsuario', '=', $idUsuario)->get();
      $categories = Categoria::where('idUsuario', '=', $idUsuario)->get();
      return view('centros')->with(['centro' => true, 'centros' => $centros, 'categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'nombre' => 'required|max:50',
          'descripcion' => 'required|max:1000',
          'direccion' => 'required|max:150',
          'latitud' => 'required|numeric',
          'longitud' => 'required|numeric',
          'horario' => 'required|max:50',
          'telefono' => 'required|numeric',
          'categoria' => 'required'
        ]);

        $express = 'No';
        if($request->express) {
          $express = 'Si';
        }
        $centro = new Centro();
        $centro->nombre = $request->nombre;
        $centro->descripcion = $request->descripcion;
        $centro->direccion = $request->direccion;
        $centro->latitud = $request->latitud;
        $centro->longitud = $request->longitud;
        $centro->horario = $request->horario;
        $centro->telefono = $request->telefono;
        $centro->express = $express;
        $centro->idUsuario = auth()->user()->id;


        DB::transaction(function () use($request, $express, $centro) {

          $centro->save();
          if(isset($request->categoria)) {
            foreach ($request->categoria as $categoria) {
              $categoria_centros = new CategoriaCentros();
              $categoria_centros->idCentro = $centro->id;
              $categoria_centros->idCategoria = $categoria;
              $categoria_centros->precio_kilo = 0;
              $categoria_centros->idUsuario = auth()->user()->id;
              $categoria_centros->save();
            }
          }
        }, 5);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $this->validate($request, [
        'id' => 'required',
        'nombre' => 'required|max:50',
        'descripcion' => 'required|max:1000',
        'direccion' => 'required|max:150',
        'latitud' => 'required|numeric',
        'longitud' => 'required|numeric',
        'horario' => 'required|max:50',
        'telefono' => 'required|numeric',
        'categoria' => 'required'
      ]);

      $express = 'No';
      if($request->express) {
        $express = 'Si';
      }
      $centro = Centro::find($request->id);
      $centro->nombre = $request->nombre;
      $centro->descripcion = $request->descripcion;
      $centro->direccion = $request->direccion;
      $centro->latitud = $request->latitud;
      $centro->longitud = $request->longitud;
      $centro->horario = $request->horario;
      $centro->telefono = $request->telefono;
      $centro->express = $express;
      $centro->idUsuario = auth()->user()->id;

      DB::transaction(function () use($request, $centro) {

        $categoriaCentros = categoriaCentros::where('idCentro', $request->id)->get();
        foreach ($categoriaCentros as $cc) {
          categoriaCentros::destroy($cc->id);
        }

        $centro->save();

        if(isset($request->categoria)) {
          foreach ($request->categoria as $categoria) {
            $categoria_centros = new CategoriaCentros();
            $categoria_centros->idCentro = $centro->id;
            $categoria_centros->idCategoria = $categoria;
            $categoria_centros->precio_kilo = 0;
            $categoria_centros->idUsuario = auth()->user()->id;
            $categoria_centros->save();
          }
        }
      }, 5);

      return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      DB::transaction(function () use($request) {

        $categoriaCentros = categoriaCentros::where('idCentro', $request->id)->get();
        foreach ($categoriaCentros as $cc) {
          categoriaCentros::destroy($cc->id);
        }
        Centro::destroy($request->id);
      }, 5);
        return back();
    }
}
