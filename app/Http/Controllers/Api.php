<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Categoria;
use \App\CategoriaCentros;
use \App\Centro;
use \App\User;

class Api extends Controller
{
    /**
     * Devuelve todas lass catagorías.
     */
    public function getCategorias()
    {
      return response()->json(Categoria::all());
    }

    /**
     * Devuelve todos los valores de la tabla categoria_centros.
     */
    public function getCategoriaCentros()
    {
      return response()->json(CategoriaCentros::all());
    }

    /**
     * Devuelve todos los entros.
     */
    public function getCentros()
    {
      return response()->json(Centro::all());
    }

    /**
     * Devuelve todos los usuarios.
     */
    public function getUsuarios()
    {
      return response()->json(User::all());
    }

    ////////////////////////////////////////////////////////////////////////////
    /**
     * Devuelve una categoria si existe.
     */
    public function getCategoriasById($id)
    {
      return response()->json(Categoria::find($id));
    }

    /**
     * Devuelve una categoria_centros si existe.
     */
    public function getCategoriaCentrosById($id)
    {
      return response()->json(CategoriaCentros::find($id));
    }

    /**
     * Devuelve un centro si existe.
     */
    public function getCentrosById($id)
    {
      return response()->json(Centro::find($id));
    }

    /**
     * Devuelve un usuario si existe
     */
    public function getUsuariosById($id)
    {
      return response()->json(User::find($id));
    }

    public function getCategoriaByCentro($idCentro) {
      $categoriaCentros = CategoriaCentros::where('idCentro', $idCentro)->get();
      
      $categoriaIds = [];
      foreach($categoriaCentros as $c) {
        array_push($categoriaIds, $c->idCategoria);
      }

      $categorias = Categoria::whereIn('id', $categoriaIds)->get();

      return response()->json($categorias);
    }
}
