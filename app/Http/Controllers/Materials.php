<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Material;

use App\Centro;

use App\Categoria;

use \DB;

use Carbon\Carbon;

class Materials extends Controller
{
    public function __construct()
  {
    $this->middleware('auth');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $idUsuario = auth()->user()->id;
        $centros = Centro::where('idUsuario', '=', $idUsuario)->get();
        $categories = Categoria::where('idUsuario', '=', $idUsuario)->get();
        $materials = Material::where('idUsuario', '=', $idUsuario)->get();
        return view('material')->with(['material' => true, 'centros' => $centros, 'categories' => $categories, 'materials' => $materials]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, 
            [
                "centro" => "required|exists:centros,id",
                "categoria" => "required|exists:categorias,id",
                "fecha" => "required|date",
                "cantidad" => "required|numeric",
                "codigo" => "required"
            ]
        );

        $time = strtotime($request->fecha);

        $newformat = date('Y-m-d', $time);

        $material = new Material();
        $material->cantidad = $request->cantidad;
        $material->fecha = $newformat;
        $material->idCentro = $request->centro;
        $material->idCategoria = $request->categoria;
        $material->codigo = $request->codigo;
        $material->idUsuario = auth()->user()->id;

        $material->save();

        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function select_centro(Request $request) 
    {
        dd($request);
    }

    public function delete_material(Request $request)
    {
        $this->validate($request, 
            [
                'id' => 'required'
            ]
        );

        Material::destroy($request->id);

        return back();
    }

    public function material_update(Request $request) 
    {
        $this->validate($request, 
            [
                'id' => 'required',
                'codigo' => 'required',
                'cantidad' => 'required|numeric',
                "fecha" => "required|date",
            ]
        );

        $time = strtotime($request->fecha);

        $newformat = date('Y-m-d', $time);

        $material = Material::find($request->id);
        $material->cantidad = $request->cantidad;
        $material->fecha = $newformat;

        $material->save();

        return back();
    }

    public function material_chart(Request $request) 
    {
        // Todos los materiales
        $this->validate($request, 
            [
                "categoria" => "required|exists:categorias,id"
            ]
        );

        $materials = Material::where('idCategoria', '=', $request->categoria)->orderBy('fecha')->get();
        // $materials = $materials->where();
        
        
        if( $request->centro != 0 ) {
            $this->validate($request, 
                [
                    "centro" => "required|exists:centros,id",
                ]
            );
            $materials = $materials->where('idCentro', '=', $request->centro);
        } else {
             $this->validate($request, 
                [
                    "centro" => "required",
                ]
            );
        }  

        if( isset($request->fecha_inicio) ) {
            $this->validate($request, 
                [
                    "fecha_inicio" => "date",
                ]
            );
            $time_1 = strtotime($request->fecha_inicio);
            $fecha_inicio = date('Y-m-d', $time_1);
            $materials = $materials->where('fecha', '>=', $fecha_inicio);
        }

        if( isset($request->fecha_final) ) {
            $this->validate($request, 
                [
                    "fecha_final" => "date",
                ]
            );
            $time_2 = strtotime($request->fecha_final);
            $fecha_final = date('Y-m-d', $time_2);
            $materials = $materials->where('fecha', '<=', $fecha_final);
        }

        $idUsuario = auth()->user()->id;
        $centros = Centro::where('idUsuario', '=', $idUsuario)->get();
        $categories = Categoria::where('idUsuario', '=', $idUsuario)->get();

        

        // Group by
        $materials = $materials->groupBy('idCategoria');

        // dd($materials);

        $max_index = 0;
        $index = 0;
        $iteration = 0;
        foreach($materials as $material)
        {
            if( $max_index < count($material) ){
                $max_index = count($material);
                $index = $iteration;
            }
            $iteration = $iteration + 1;
        }

        $count = 0;
        $labels = [];
        $fecha_inicio = "";
        $fecha_final = "";
  
        foreach($materials as $material) 
        {
            foreach($material as $m) 
            {
                if( !in_array($m->fecha, $labels) ) 
                {
                    array_push($labels, $m->fecha);
                }
            }
        }

        $labels = array_sort_recursive($labels);
        

        return view('chart')->with(['centros' => $centros, 'categories' => $categories, 'materials' => $materials, 'charjs' => True, 'labels' => $labels]);
    }
}
