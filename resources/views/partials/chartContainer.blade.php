

<canvas id="myChart" width="400" height="180"></canvas>
<script>


var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [
            @foreach($labels as $l)
                '{{ $l }}',
            @endforeach
        ],
        datasets: [
            @foreach($materials as $material)
                <?php 
                    $fecha = "";
                    $cantidad = 0;
                    $count = 0;
                ?>
                {
                label:
                    @foreach($categories as $categoria)
                        @if($categoria->id == $material[0]->idCategoria )
                            '{{ $categoria->descripcion }}'
                        @endif
                    @endforeach
                ,
                data: [
                        <?php
                            $material = collect($material);
                            $material = $material->groupBy('fecha');
                        ?>
                        @foreach($material as $ma)
                            <?php
                                $cantidad = 0;
                            ?>
                            @foreach($ma as $m)
                                <?php
                                    $cantidad = $cantidad + $m->cantidad;
                                ?>
                            @endforeach
                            {{ $cantidad }},
                        @endforeach
                    ],
                backgroundColor: [
                    @foreach($material as $m)
                    'rgba( {{rand(0,255)}} , {{rand(0,255)}}, {{rand(0,255)}}, 0.5)',
                    @endforeach
                ],
                borderColor: [
                    @foreach($material as $m)
                    'rgba({{rand(0,255)}}, {{rand(0,255)}}, {{rand(0,255)}}, 1)',
                    @endforeach
                ],
                borderWidth: 0.1
            },
            @endforeach
        ]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});

</script>