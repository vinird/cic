@if(isset($centros))
<div class="table-responsive">
  <table class="table table-hover">
    <thead></thead>
      <th>Nombre</th>
      <th>Descripción</th>
      <th>Dirección</th>
      <th>Latitud</th>
      <th>Longitud</th>
      <th>Horario</th>
      <th>Express</th>
      <th>Teléfono</th>
      <th></th>
    </thead>
    <tbody>
      @foreach($centros as $centro)
      <tr>
        <td>{{ $centro->nombre }}</td>
        <td>{{ $centro->descripcion }}</td>
        <td>{{ $centro->direccion }}</td>
        <td>{{ $centro->latitud }}</td>
        <td>{{ $centro->longitud }}</td>
        <td>{{ $centro->horario }}</td>
        <td>{{ $centro->express }}</td>
        <td>{{ $centro->telefono }}</td>
        <td>

            <button type="submit" class="btn btn-xs btn-danger" onclick="button_delete_centro( '{{ $centro->id }}', '{{ $centro->nombre }}' )" name="button">Eliminar</button>
            <button type="button" class="btn btn-xs  btn-warning" onclick="button_update_centro(
              '{{ $centro->id }}',
              '{{ $centro->nombre }}',
              '{{ $centro->descripcion }}',
              '{{ $centro->direccion }}',
              '{{ $centro->latitud }}',
              '{{ $centro->longitud }}',
              '{{ $centro->horario }}',
              '{{ $centro->telefono }}'
              )" name="button">Modificar</button>

        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
@if(count($centros) == 0)
<div class="text-center">
  <h4>No hay datos</h4>
</div>
@endif
@endif
