<div class="collapse" id="collapseExample">
  <div class="well">
    @if(isset($categories))
    @if(count($categories) > 0)
    <form class="form-horizontal" action="{{ url('/centro') }}" method="POST">
      {{ csrf_field() }}
      <div class="form-group form-group-sm">
        <label for="nombre" class="col-sm-2 control-label">Nombre</label>
        <div class="col-sm-10 col-md-8 ">
          <input type="text" class="form-control" name="nombre" placeholder="Nombre del centro..." value="{{ old('nombre') }}" maxlength="50" required>
        </div>
      </div>
      <div class="form-group form-group-sm">
        <label for="descripcion" class="col-sm-2 control-label">Descripción</label>
        <div class="col-sm-10 col-md-8 ">
          <input type="text" class="form-control" name="descripcion" placeholder="Descripción del centro..." value="{{ old('descripcion') }}" maxlength="1000" required>
        </div>
      </div>
      <div class="form-group form-group-sm">
        <label for="direccion" class="col-sm-2 control-label">Dirección</label>
        <div class="col-sm-10 col-md-8 ">
          <input type="text" class="form-control" name="direccion" placeholder="Dirección física..." value="{{ old('direccion') }}" maxlength="150" required>
        </div>
      </div>
      <hr>
      <div class="col-sm-offset-2 col-sm-10 col-md-8 ">
        <a href="http://www.latlong.net/" target="_blank">Consultar Mapa ?</a>
      </div>
      <br>
      <div class="form-group form-group-sm">
        <label for="latitud" class="col-sm-2 control-label">Latittud</label>
        <div class="col-sm-10 col-md-8 ">
          <input type="text" class="form-control" name="latitud" value="{{ old('latitud') }}" required>
        </div>
      </div>
      <div class="form-group form-group-sm">
        <label for="longitud" class="col-sm-2 control-label">Longitud</label>
        <div class="col-sm-10 col-md-8 ">
          <input type="text" class="form-control" name="longitud" value="{{ old('longitud') }}" required>
        </div>
      </div>
      <hr>
      <div class="form-group form-group-sm">
        <label for="horario" class="col-sm-2 control-label">Horario</label>
        <div class="col-sm-10 col-md-8 ">
          <input type="text" class="form-control" name="horario" placeholder="Horario de atención..." value="{{ old('horario') }}" maxlength="50" required>
        </div>
      </div>
      <div class="form-group form-group-sm">
        <label for="telefono" class="col-sm-2 control-label">Teléfono</label>
        <div class="col-sm-10 col-md-8 ">
          <input type="text" class="form-control" name="telefono" placeholder="Número de télefon..." value="{{ old('telefono') }}" maxlength="20" pattern="[0-9]+" required>
        </div>
      </div>

      <div class="form-group form-group-sm">
        <label for="categories" class="col-sm-2 control-label">Categorías</label>
        <div class="col-sm-10 col-md-8">
          <select name="categoria[]"  multiple class="form-control" id="select_multiple_categpries" required>
            @foreach($categories as $category)
            <option value="{{ $category->id }}">{{ $category->descripcion }}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group form-group-sm">
        <div class="col-sm-offset-2 col-sm-10 col-md-8 ">
          <div class="checkbox">
            <label>
              <input type="checkbox" name="express"> Realizan servicio express
            </label>
          </div>
        </div>
      </div>

      <div class="form-group form-group-sm">
        <div class="col-sm-offset-2 col-sm-10 col-md-8 ">
          <button type="submit" class="btn btn-primary">Agregar</button>
        </div>
      </div>
    </form>
    @else
    <div class="text-center">
      <h4>Debe agregar categorías para crear centros</h4>
    </div>
    @endif
    @else
    <div class="text-center">
      <h4>Debe agregar categorías para crear centros</h4>
    </div>
    @endif
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#select_multiple_categpries').multiselect();
    });
</script>
