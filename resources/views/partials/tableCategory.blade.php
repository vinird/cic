@if(isset($categories))
<table class="table table-hover table-responsive">
  <thead>
    <th>Descripción</th>
    <th></th>
    <th></th>
  </thead>
  <tbody>
    @foreach($categories as $category)
    <tr>
      <td>{{ $category->descripcion }}</td>
      <td><span class="label"  style="background:{{ $category->color }};">Color</span></td>
      <td>

          <button type="submit" class="btn btn-xs btn-danger" onclick="button_delete_category( '{{ $category->id }}', '{{ $category->descripcion }}' )" name="button">Eliminar</button>
          <button type="button" class="btn btn-xs  btn-warning" onclick="button_update_category( '{{ $category->id }}', '{{ $category->descripcion }}', '{{ $category->color }}' )" name="button">Modificar</button>

      </td>
    </tr>
    @endforeach
  </tbody>
</table>
@if(count($categories) == 0)
<div class="text-center">
  <h4>No hay datos</h4>
</div>
@endif
@endif
