
<!-- Modal ver graficos material -->
<div class="modal fade" id="seeMaterialChart" tabindex="-1" role="dialog" aria-labelledby="seeMaterialChart">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form class="form-horizontal" action="{{ url('/material-chart') }}" method="POST">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          {{ csrf_field() }}

            @if(isset($centros) && isset($categories))
                @if(count($centros) && count($centros))
                <div class="form-group form-group-sm">
                    <label for="centros" class="col-sm-3 control-label">Centro:</label>
                    <div class="col-sm-9 col-md-7">
                    <select name="centro"  class="form-control">
                        @foreach($centros as $centro)
                        <option value="{{ $centro->id }}" required>{{ $centro->nombre }}</option>
                        @endforeach
                    </select>
                    </div>
                </div>

                <div class="form-group form-group-sm">
                    <label for="categories" class="col-sm-3 control-label">Categorías:</label>
                    <div class="col-sm-9 col-md-7">
                    <select name="categoria"  class="form-control" required>
                        @foreach($categories as $category)
                        <option value="{{ $category->id }}" >{{ $category->descripcion }}</option>
                        @endforeach
                    </select>
                        <span class="help-block">Debe seleccionar una categoría<span>                        
                    </div>
                </div>

                <div class="form-group form-group-sm">
                    <label for="fecha_inicio" class="col-sm-3 control-label">Fecha inicio:</label>
                    <div class="col-sm-9 col-md-7 ">
                        <input type="date" class="form-control" name="fecha_inicio">
                        <span class="help-block">Formato: 12/12/2018<span>
                    </div>
                </div>

                <div class="form-group form-group-sm">
                    <label for="fecha_final" class="col-sm-3 control-label">Fecha final:</label>
                    <div class="col-sm-9 col-md-7 ">
                        <input type="date" class="form-control" name="fecha_final">
                        <span class="help-block">Formato: 12/12/2018<span>
                    </div>
                </div>

                
                @endif
                @else
                    <div class="text-center">
                        <h4>Debe ingresar centros y categorías</h4>
                    </div>
                @endif

        </div>
        <div class="modal-footer">

            <a type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</a>
            <button type="submit" class="btn btn-primary btn-sm">Mostrar gráfico</button>

        </div>
      </form>
    </div>
  </div>
</div>


<script type="text/javascript">
  function see_material_chart()
  {
    $('#seeMaterialChart').modal('toggle')
  }

</script>