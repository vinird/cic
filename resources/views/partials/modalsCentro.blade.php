

<!-- Modal eliminar centro -->
<div class="modal fade" id="deleteCentro" tabindex="-1" role="dialog" aria-labelledby="deleteCategory">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body text-center">
        <h4>¿Eliminar centro <strong id="centro_delete_nombre"></strong>?</h4>
      </div>
      <div class="modal-footer">

        <form id="form_delete_centro" action="{{ url('/delete-centro') }}" method="post">
            {{ csrf_field() }}
          <input type="text" name="id" id="centro_delete_id" class="hidden">
          <a type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</a>
          <button type="submit" class="btn btn-danger btn-sm">Eliminar</button>
        </form>

      </div>
    </div>
  </div>
</div>

<!-- Modal actualizar centro -->
<div class="modal fade" id="updateCentro" tabindex="-1" role="dialog" aria-labelledby="updateCentro">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form class="form-horizontal" action="{{ url('/centro') }}" method="POST">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          {{ csrf_field() }}
          <input type="hidden" name="_method" value="PUT">
          <input type="text" name="id" class="hidden" id="centro_update_id">
          <div class="form-group form-group-sm">
            <label for="nombre" class="col-sm-2 control-label">Nombre</label>
            <div class="col-sm-10 col-md-8 ">
              <input type="text" class="form-control" name="nombre" maxlength="50" id="centro_update_nombre">
            </div>
          </div>
          <div class="form-group form-group-sm">
            <label for="descripcion" class="col-sm-2 control-label">Descripción</label>
            <div class="col-sm-10 col-md-8 ">
              <input type="text" class="form-control" name="descripcion" maxlength="1000" id="centro_update_descripcion">
            </div>
          </div>
          <div class="form-group form-group-sm">
            <label for="direccion" class="col-sm-2 control-label">Dirección</label>
            <div class="col-sm-10 col-md-8 ">
              <input type="text" class="form-control" name="direccion" maxlength="150" id="centro_update_direccion">
            </div>
          </div>
          <hr>
          <div class="col-sm-offset-2 col-sm-10 col-md-8 ">
            <a href="http://www.latlong.net/" target="_blank">Consultar Mapa ?</a>
          </div>
          <br>
          <div class="form-group form-group-sm">
            <label for="latitud" class="col-sm-2 control-label">Latittud</label>
            <div class="col-sm-10 col-md-8 ">
              <input type="text" class="form-control" name="latitud" id="centro_update_latitud">
            </div>
          </div>
          <div class="form-group form-group-sm">
            <label for="longitud" class="col-sm-2 control-label">Longitud</label>
            <div class="col-sm-10 col-md-8 ">
              <input type="text" class="form-control" name="longitud" id="centro_update_longitud">
            </div>
          </div>
          <hr>
          <div class="form-group form-group-sm">
            <label for="horario" class="col-sm-2 control-label">Horario</label>
            <div class="col-sm-10 col-md-8 ">
              <input type="text" class="form-control" name="horario" maxlength="50" id="centro_update_horario">
            </div>
          </div>
          <div class="form-group form-group-sm">
            <label for="telefono" class="col-sm-2 control-label">Teléfono</label>
            <div class="col-sm-10 col-md-8 ">
              <input type="text" class="form-control" name="telefono" maxlength="20" pattern="[0-9]+" id="centro_update_telefono">
            </div>
          </div>
          @if(isset($categories))
          @if(count($categories))
          <div class="form-group form-group-sm">
            <label for="categories" class="col-sm-2 control-label">Categorías</label>
            <div class="col-sm-10 col-md-8">
              <select name="categoria[]"  multiple class="form-control" id="select_multiple_categories_modal">
                @foreach($categories as $category)
                <option value="{{ $category->id }}">{{ $category->descripcion }}</option>
                @endforeach
              </select>
            </div>
          </div>
          @endif
          @endif
          <div class="form-group form-group-sm">
            <div class="col-sm-offset-2 col-sm-10 col-md-8 ">
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="express"> Realizan servicio express
                </label>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">

            <a type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</a>
            <button type="submit" class="btn btn-warning btn-sm">Actualizar</button>

        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">

  function button_delete_centro(id, nombre){
    $('#centro_delete_id').val(id);
    $('#centro_delete_nombre').text(nombre);
    $('#deleteCentro').modal('toggle')
  }

  function button_update_centro(
    id,
    nombre,
    descripcion,
    direccion,
    latitud,
    longitud,
    horario,
    telefono
  ){
    $('#centro_update_id').val(id);
    $('#centro_update_nombre').val(nombre);
    $('#centro_update_descripcion').val(descripcion);
    $('#centro_update_direccion').val(direccion);
    $('#centro_update_latitud').val(latitud);
    $('#centro_update_longitud').val(longitud);
    $('#centro_update_horario').val(horario);
    $('#centro_update_telefono').val(telefono);
    $('#updateCentro').modal('toggle')
  }

  $(document).ready(function() {
    $('#select_multiple_categories_modal').multiselect();
  });

</script>
