<div class="">
    <div class="table-responsive">
        <table class="table table-responsive" id="table-materials">
            <thead>
                <th>Código</th>
                <th>Fecha</th>
                <th>Centro</th>
                <th>Categoría</th>
                <th>Cantidad</th>
                <th></th>
            </thead>
            <tbody>
                @if(isset($materials))
                @foreach($materials as $material)
                <tr>
                    <td>{{ $material->codigo }}</td>
                    <td>{{ $material->fecha }}</td>
                    <td>
                        @foreach($centros as $centro)
                            @if($centro->id == $material->idCentro)
                                {{ $centro->nombre }}
                            @endif
                        @endforeach
                    </td>
                    <td>
                        @foreach($categories as $category)
                            @if($category->id == $material->idCategoria)
                                {{ $category->descripcion }}
                            @endif
                        @endforeach
                    </td>
                    <td>{{ $material->cantidad }} Kg</td>
                    <td>
                        <a href="#" onclick="button_delete_material( '{{ $material->id }}', '{{ $material->codigo }}' );" class="btn btn-xs btn-danger" >Eliminar</a>
                        <a href="#" onclick="button_update_material( '{{ $material->id }}' ,'{{ $material->codigo }}', '{{ $material->cantidad }}', '{{ $material->fecha }}' )" class="btn btn-xs btn-warning" >Modificar</a>
                    </td>
                </tr>
                @endforeach
                @else
                    <p class="text-center">
                        <h3>No hay datos</h3>
                    </p>
                @endif
            </tbody>
            
        </table>
    </div>
</div>
<style>
table.dataTable thead .sorting:after,
table.dataTable thead .sorting_asc:after,
table.dataTable thead .sorting_desc:after {
    display: none!important;
}
</style>
<script>
    $(document).ready(function() {
        $('#table-materials').dataTable( {
            "language": {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        } );
    } );

</script>