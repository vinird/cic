<form method="POST" autocomplete="off" action="{{ url('/category') }}">
  {{ csrf_field() }}
  <div class="form-group">
    <label for="description">Descripción</label>
    <input type="text" class="form-control" name="descripcion" placeholder="Nombre de la categoría..." maxlength="150" value="{{ old('description')}}" required>
  </div>
  <div class="form-group">
    <label for="color">Color</label>
    <input type="color" class="form-control" name="color" maxlength="10" required>
  </div>
  <button type="submit" class="btn btn-default">Agregar</button>
</form>
