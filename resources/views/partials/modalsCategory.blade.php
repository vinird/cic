

<!-- Modal eliminar categoría -->
<div class="modal fade" id="deleteCategory" tabindex="-1" role="dialog" aria-labelledby="deleteCategory">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body text-center">
        <h4>¿Eliminar categoría <strong id="category_delete_description"></strong>?</h4>
      </div>
      <div class="modal-footer">

        <form id="form_delete_category" action="{{ url('/delete-category') }}" method="post">
            {{ csrf_field() }}
          <input type="text" name="id" id="category_delete_id" class="hidden">
          <a type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</a>
          <button type="submit" class="btn btn-danger btn-sm">Eliminar</button>
        </form>

      </div>
    </div>
  </div>
</div>

<!-- Modal actualizar categoría -->
<div class="modal fade" id="updateCategory" tabindex="-1" role="dialog" aria-labelledby="deleteCategory">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form id="form_delete_category" action="{{ url('/category') }}" method="post">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-xs-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
              {{ csrf_field() }}
              <input type="hidden" name="_method" value="PUT">
              <input type="text" name="id" id="category_update_id" class="hidden">
              <div class="form-group">
                <label for="description">Descripción</label>
                <input type="text" class="form-control" name="descripcion" id="category_update_description" maxlength="150" value="{{ old('description')}}" required>
              </div>
              <div class="form-group">
                <label for="color">Color</label>
                <input type="color" class="form-control" id="category_update_color" name="color" maxlength="10" required>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">

            <a type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</a>
            <button type="submit" class="btn btn-warning btn-sm">Actualizar</button>

        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">

  function button_delete_category(id, descripcion){
    $('#category_delete_id').val(id);
    $('#category_delete_description').text(descripcion);
    $('#deleteCategory').modal('toggle')
  }

  function button_update_category(id, descripcion, color){
    $('#category_update_id').val(id);
    $('#category_update_description').val(descripcion);
    $('#category_update_color').val(color);
    $('#updateCategory').modal('toggle')
  }

</script>
