

<!-- Modal eliminar material -->
<div class="modal fade" id="deleteMaterial" tabindex="-1" role="dialog" aria-labelledby="deleteMaterial">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body text-center">
              <h4>¿Eliminar material <strong id="material_delete_nombre"></strong>?</h4>
            </div>
            <div class="modal-footer">

              <form id="form_delete_centro" action="{{ url('/delete-material') }}" method="post">
                  {{ csrf_field() }}
                <input type="text" name="id" id="material_delete_id" class="hidden">
                <a type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</a>
                <button type="submit" class="btn btn-danger btn-sm">Eliminar</button>
              </form>

            </div>
        </div>
    </div>
</div>

<!-- Modal actualizar material -->
<div class="modal fade" id="updatematerial" tabindex="-1" role="dialog" aria-labelledby="updatematerial">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form class="form-horizontal" action="{{ url('/material-update') }}" method="POST">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          {{ csrf_field() }}
          <input type="text" class="form-control material_update_nombre hidden" name="codigo" required>
          <input type="text" name="id" class="hidden" id="material_update_id">

          <div class="form-group form-group-sm">
            <label for="nombre" class="col-sm-3 control-label">Codigo</label>
            <div class="col-sm-9 col-md-8 ">
              <input type="text" class="form-control material_update_nombre" disabled>
            </div>
          </div>

          <div class="form-group form-group-sm">
              <label for="fecha" class="col-sm-3 control-label">Fecha</label>
              <div class="col-sm-9 col-md-7 ">
                  <input type="date" class="form-control" name="fecha" id="material_update_fecha" required>
                  <span class="help-block">Formato: 12/12/2018<span>
              </div>
          </div>

          <div class="form-group form-group-sm">
            <label for="nombre" class="col-sm-3 control-label">Cantidad</label>
            <div class="col-sm-9 col-md-8 input-group">
              <span class="input-group-addon">Kg</span>
              <input type="text" class="form-control" name="cantidad" maxlength="50" id="material_update_cantidad" required>
            </div>
          </div>

        </div>
        <div class="modal-footer">

            <a type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</a>
            <button type="submit" class="btn btn-warning btn-sm">Actualizar</button>

        </div>
      </form>
    </div>
  </div>
</div>


<script type="text/javascript">

  function button_delete_material(id, nombre){
    $('#material_delete_id').val(id);
    $('#material_delete_nombre').text(nombre);
    $('#deleteMaterial').modal('toggle');
  }

  function button_update_material(
    id,
    nombre,
    cantidad,
    fecha
  ){
    $('#material_update_id').val(id);
    $('.material_update_nombre').val(nombre);
    $('#material_update_cantidad').val(cantidad);
    $('#material_update_fecha').val(fecha);
    $('#updatematerial').modal('toggle')
  }



</script>
