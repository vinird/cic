<?php
    $current_time = time();
    $code = $current_time . "-" . strtoupper(str_random(5));
?>

<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'CIC') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <style media="screen">
      body{
      background:url('cic/images/header.jpg');
      background-size:100% 100%;
      background-attachment: fixed;
      background-repeat:no-repeat;
      font-family: 'Open Sans', sans-serif;
      padding-bottom: 40px;
      }
    </style>

    @if(isset($centro))
      <link rel="stylesheet" href="{{ asset('css/bootstrap-multiselect.css') }}">
    @endif
    @if(isset($material))
        <link rel="stylesheet" href="{{ asset('css/datatables.min.css') }}">
    @endif
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    @if(isset($charjs))
        <script src="{{ asset('js/Chart.bundle.min.js') }}"></script>
    @endif
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'CIC') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                        <li class="@if(isset($category)) active @endif"><a href="{{ url('category') }}">Categorías <span class="sr-only">(current)</span></a></li>
                        <li class="@if(isset($centro)) active @endif">
                          <a href="{{ url('centro') }}">Centros <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="@if(isset($material)) active @endif">
                          <a href="{{ url('/material') }}" >Materiales <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="">
                          <a href="#" data-toggle="modal" data-target="#material-modal-selection">Agregar material <span class="sr-only">(current)</span></a>
                        </li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Ingresar</a></li>
                            <li><a href="{{ route('register') }}">Registrarse</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Salir
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    @if(isset($centro))
    <script src="{{ asset('js/bootstrap-multiselect.js') }}"></script>
    @endif
    @if(isset($material))
        <script src="{{ asset('js/datatables.min.js') }}"></script>
    @endif

    <!-- Modal -->
    <div class="modal fade" id="material-modal-selection" tabindex="-1" role="dialog" aria-labelledby="material-modal-selectionLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="material-modal-selectionLabel">Agregar material</h4>
        </div>
        <form method="POST" action="{{ url('/material') }}" class="form-horizontal">
            {{ csrf_field() }}
            <input type="text" name="codigo" value="{{ $code }}" class="hidden">
            <div class="modal-body">

                @if(isset($centros) && isset($categories))
                @if(count($centros) && count($centros))
                <div class="form-group form-group-sm">
                    <label for="centros" class="col-sm-3 control-label">Centro:</label>
                    <div class="col-sm-9 col-md-7">
                    <select name="centro"  class="form-control">
                        @foreach($centros as $centro)
                        <option value="{{ $centro->id }}" required>{{ $centro->nombre }}</option>
                        @endforeach
                    </select>
                    </div>
                </div>

                <div class="form-group form-group-sm">
                    <label for="codigo" class="col-sm-3 control-label">Código</label>
                    <div class="col-sm-9 col-md-7 ">
                        <input type="text" class="form-control codigo-material" value="{{ $code }}" required disabled>
                    </div>
                </div>

                <div class="form-group form-group-sm">
                    <label for="categories" class="col-sm-3 control-label">Categorías</label>
                    <div class="col-sm-9 col-md-7">
                    <select name="categoria"  class="form-control" >
                        @foreach($categories as $category)
                        <option value="{{ $category->id }}" required>{{ $category->descripcion }}</option>
                        @endforeach
                    </select>
                    </div>
                </div>

                <div class="form-group form-group-sm">
                    <label for="fecha" class="col-sm-3 control-label">Fecha</label>
                    <div class="col-sm-9 col-md-7 ">
                        <input type="date" class="form-control" name="fecha" required>
                        <span class="help-block">Formato: 12/12/2018<span>
                    </div>
                </div>

                <div class="form-group form-group-sm">
                    <label for="cantidad" class="col-sm-3 control-label">Cantidad</label>
                    <div class="col-sm-9 col-md-7 input-group">
                        <span class="input-group-addon">Kg</span>
                        <input type="number" class="form-control" name="cantidad" required>
                    </div>
                </div>

                @else
                    <div class="text-center">
                        <h4>Debe ingresar centros y categorías</h4>
                    </div>
                @endif
                @endif

                <br>
            </div>
            <div class="modal-footer">
                <a class="btn btn-sm btn-default" data-dismiss="modal">Cerrar</a>
                <button type="submit" class="btn btn-sm btn-primary" required>Agregar material</button>
            </div>
        </form>
        </div>
    </div>
    </div>
</body>
</html>
