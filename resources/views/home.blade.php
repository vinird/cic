@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      @if (count($errors) > 0)
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
      @endif
      @if(isset($alert))
        <div class="alert alert-danger">
            <ul>
                <li>{{ $alert }}</li>
            </ul>
        </div>
      @endif
    </div>
  </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">Home</div>

                <div class="panel-body text-center">
                  <img src="{{ asset('cic/images/cic_logo2.jpg') }}" class="img-responsive img-rounded center-block" alt="cic logo">
                  <hr>
                  <p>
                    Sistema para recopilación de datos y ubicación de centros de acopio.
                  </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
