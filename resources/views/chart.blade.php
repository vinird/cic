@extends('layouts.app')

@section('content')
<div class="container-fluid">
  @if(isset($alert))
    <div class="alert alert-danger">
        <ul>
            <li>{{ $alert }}</li>
        </ul>
    </div>
  @endif
  <div class="row">
    <div class="col-xs-12">
      @if (count($errors) > 0)
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
      @endif
    </div>
  </div>
    <div class="row">
        <div class="col-xs-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                  Gráfico
                  <div class="pull-right"><a href="#" onclick="see_material_chart()">Ver gráficos</a></div>
                </div>

                <div class="panel-body">
                    @include('partials.chartContainer')
                </div>
            </div>
        </div>
    </div>
</div>
@include('partials.modalsMaterials')
@include('partials.modalChart')
@endsection
