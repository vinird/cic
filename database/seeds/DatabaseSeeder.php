<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        \DB::table('users')->insert([
          'id' => 1,
          'name' => 'admin',
          'apellido1' => 'apellido1',
          'apellido2' => 'apellido2',
          'email' => 'admin@admin.com',
          'password' => bcrypt('password')
        ]);

        \DB::table('centros')->insert([
          'id' => 1,
          'nombre' => 'Tecno Ambiente',
          'descripcion' => 'Centro de reciclaje del pacífico',
          'direccion' => 'Puntarenas, Miramar',
          'latitud' => 10.135295,
          'longitud' => -84.907338,
          'horario' => '8am - 5pm',
          'express' => 'SI',
          'telefono' => '+506 9812 12 21',
          'idUsuario' => 1
        ]);

        \DB::table('centros')->insert([
          'id' => 2,
          'nombre' => 'Eco Ideas',
          'descripcion' => 'Centro de reciclaje del caribe',
          'direccion' => 'Limón',
          'latitud' => 10.286376,
          'longitud' => -83.493347,
          'horario' => '8am - 5pm',
          'express' => 'SI',
          'telefono' => '+506 9812 12 21',
          'idUsuario' => 1
        ]);

        \DB::table('categorias')->insert([
          'id' => 1,
          'descripcion' => 'Categoría 1',
          'idUsuario' => 1
        ]);

        \DB::table('categorias')->insert([
          'id' => 2,
          'descripcion' => 'Categoría 2',
          'idUsuario' => 1
        ]);

        \DB::table('categorias')->insert([
          'id' => 3,
          'descripcion' => 'Categoría 3',
          'idUsuario' => 1
        ]);

        \DB::table('categoria_centros')->insert([
          'precio_kilo' => 2000,
          'idCentro' => 1,
          'idCategoria' => 1,
          'idUsuario' => 1
        ]);
    }
}
