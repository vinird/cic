<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/**
 * Rutas para categorías
 */
Route::resource('/category', 'Categories');
Route::post('/delete-category', 'Categories@destroy');
Route::put('/category', 'Categories@update');

/**
 * Rutas para centros
 */
Route::resource('/centro', 'Centros');
Route::post('/delete-centro', 'Centros@destroy');
Route::put('/centro', 'Centros@update');

Route::resource('/material', 'Materials');
Route::post('/select_centro', 'Materials@select_centro');
Route::post('/delete-material', 'Materials@delete_material');
Route::post('/material-update', 'Materials@material_update');
Route::post('/material-chart', 'Materials@material_chart');