<?php

use Illuminate\Http\Request;
use \App\Categoria;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/categoriaCentros', 'Api@getCategoriaCentros');
Route::get('/categorias', 'Api@getCategorias');
Route::get('usuarios', 'Api@getUsuarios');
Route::get('/centros', 'Api@getCentros');

Route::get('/categoriaCentros/{id}', 'Api@getCategoriaCentrosById');
Route::get('/categorias/{id}', 'Api@getCategoriasById');
Route::get('usuarios/{id}', 'Api@getUsuariosById');
Route::get('/centros/{id}', 'Api@getCentrosById');

Route::get('/categoriaByCentro/{id}', 'Api@getCategoriaByCentro'); 

Route::post('/test', function (Request $request)
{
  return $request;
});
